# BoidFlock

Simple Boid Flock simulator in Unity3D.

# Introduction
This is a setup project for experimenting with the Unity3D ECS. It is based on this a project made by [Sebastian Lague](https://github.com/SebLague/Boids).
I added some graphics and made some small changes to the code in the project.
I also added a model with a skinned mesh renderer and an animator for the Boids to try and simulate a more realistic situation when porting something to the ECS.
Additionally, I added a class that replaces the behaviour of the compute shader that is used to calculate certain values of the flock since it is not supported on my own hardware.