﻿using System.Collections.Generic;
using HamerSoft.BoidFlock;
using UnityEngine;

public class Flock : HsObject
{
    const int threadGroupSize = 1024;

    public BoidSettings Settings;
    public ComputeShader Compute;
    public Boid prefab;
    public float spawnRadius = 10;
    public int spawnCount = 10;
    private List<Boid> _boids;
    private Compute _compute;
    private BoidData[] _boidData;

    protected override void Start()
    {
        base.Start();
        SpawnBoids();
        if (SystemInfo.graphicsDeviceVersion.ToLower().Contains("opengl"))
            _compute = new Compute();
    }

    private void SpawnBoids()
    {
        _boids = new List<Boid>();
        for (int i = 0; i < spawnCount; i++)
        {
            var thisTransform = transform;
            Vector3 pos = thisTransform.position + Random.insideUnitSphere * spawnRadius;
            Boid boid = Instantiate(prefab, thisTransform);
            boid.transform.position = pos;
            boid.transform.forward = Random.insideUnitSphere;
            boid.SetColour(new Color(Random.value, Random.value, Random.value, 1));
            boid.Initialize(Settings, null);
            _boids.Add(boid);
        }
        _boidData = new BoidData[spawnCount];
    }

    private void Update()
    {
        if (_boids != null)
        {
            for (int i = 0; i < _boids.Count; i++)
            {
                _boidData[i].position = _boids[i].Position;
                _boidData[i].direction = _boids[i].Forward;
            }
            _boidData = ComputeBoidData(_boidData, _boidData.Length);
            for (int i = 0; i < _boids.Count; i++)
            {
                _boids[i].UpdateFlockProperties(_boidData[i].flockHeading,_boidData[i].flockCentre,_boidData[i].avoidanceHeading,_boidData[i].numFlockmates);
                _boids[i].UpdateBoid();
            }
        }
    }

    private BoidData[] ComputeBoidData(BoidData[] boidData, int numBoids)
    {
        if (_compute != null)
            boidData = _compute.ComputeData(boidData, Settings.perceptionRadius, Settings.avoidanceRadius);
        else
        {
            var boidBuffer = new ComputeBuffer(numBoids, BoidData.Size);
            boidBuffer.SetData(boidData);
            Compute.SetBuffer(0, "boids", boidBuffer);
            Compute.SetInt("numBoids", _boids.Count);
            Compute.SetFloat("viewRadius", Settings.perceptionRadius);
            Compute.SetFloat("avoidRadius", Settings.avoidanceRadius);
            int threadGroups = Mathf.CeilToInt(numBoids / (float) threadGroupSize);
            Compute.Dispatch(0, threadGroups, 1, 1);
            boidBuffer.GetData(boidData);
            boidBuffer.Release();
        }

        return boidData;
    }

    public struct BoidData
    {
        public Vector3 position;
        public Vector3 direction;

        public Vector3 flockHeading;
        public Vector3 flockCentre;
        public Vector3 avoidanceHeading;
        public int numFlockmates;

        public static int Size
        {
            get { return sizeof(float) * 3 * 5 + sizeof(int); }
        }
    }
}