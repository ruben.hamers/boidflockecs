﻿using UnityEngine;

public class Compute
{
    public Flock.BoidData[] ComputeData(Flock.BoidData[] boids, float viewRadius,
        float avoidRadius)
    {
        for (int i = 0; i < boids.Length; i++)
        {
            Flock.BoidData boidB = boids[i];
            Vector3 offset = boidB.position - boids[i].position;
            float sqrDst = offset.x * offset.x + offset.y * offset.y + offset.z * offset.z;

            if (sqrDst < viewRadius * viewRadius)
            {
                boids[i].numFlockmates += 1;
                boids[i].flockHeading += boidB.direction;
                boids[i].flockCentre += boidB.position;

                if (sqrDst < avoidRadius * avoidRadius)
                    boids[i].avoidanceHeading -= offset / sqrDst;
            }
        }

        return boids;
    }
}